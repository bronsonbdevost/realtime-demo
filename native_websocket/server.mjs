import { WebSocketServer } from 'ws'

const wss = new WebSocketServer({ port: 9883 })

wss.on('connection', function connection(ws) {
    ws.on('error', console.error)

    ws.on('message', function message(data) {
        console.log('received: %s', data)
        ws.send(`Hello Client! I got your message ${data}`)
    })

    ws.send('something')
})