# Native Websocket

Usually you will want to use some library to abstract away the underlying realtime transport. Here, however, we will look directly at the websocket implementation using the native browser API.

## Server

We will use the npm `ws` server. To get started run `npm install` and then `npm start`

## Client

Nothing special is needed for the client.
