# Basic Browser Messaging

Information can be sent between browser tabs/windows on the same domain. This is a simple example that demonstrates how web messaging typically works:

```javascript
const channel = 'test-channel'
const bc = new BroadcastChannel(channel)
bc.onmessage = (event) => {
  console.log(event)
  console.log(event.data)
}
bc.postMessage("This is a test message.")
```

The message can be any Javascript type that can be cloned (so no sending functions).