# Realtime Demo

I've collected here some resources for beginning to work with realtime web communication technology.


## Getting started

You will usually want a *nix terminal on your computer.

| Tool | Windows | Mac |
|---|---|---|
|Terminal | Power Shell (built in) | Terminal (built in) or better [iTerm 2](https://iterm2.com/downloads.html) |
| Package manager | [Windows Subsystem for Linux](https://ubuntu.com/desktop/wsl) | [Homebrew](https://brew.sh) |
| Git | `sudo apt install git-all` | `brew install git` (but it is probably already there) |
| VSCode | https://code.visualstudio.com/#alt-downloads | https://code.visualstudio.com/#alt-downloads |
| node/npm | `sudo apt install nodejs` | `brew install node` |
| Live Server | VSCode Live Server extension | VSCode Live Server extension |

Windows native (alt) install: use the [native Git package](https://gitforwindows.org), then install [VSCode](https://code.visualstudio.com/#alt-downloads), then install [node/npm](https://nodejs.org/en/download).
