// Import required files
import { existsSync, readFileSync } from 'fs'
import express from 'express'
import { Server } from 'socket.io'
import colors from 'colors'
import path from 'path'
import https from 'https'
import http from 'http'
import { fileURLToPath } from 'url'

// Initializations
colors.enable()
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

// Get SSL certs
const secretsFile = './secret.json'
const settingsString = existsSync(secretsFile) ? readFileSync(secretsFile)?.toString() : '{"cert": null, "key": null}'
const settings = JSON.parse(settingsString)
const certPath = settings?.cert ?? null
const keyPath = settings?.key ?? null
const runHttps = (certPath !== null && keyPath !== null)

// Setup basic express server
const app = express()
const server = runHttps ? https.createServer({
    key: readFileSync(keyPath),
    cert: readFileSync(certPath),
}, app) : http.createServer(app)
const io = new Server(server)
const port = process.env.PORT || (runHttps ? 443 : 8080)

server.listen(port, () => {
    console.log(`Server listening at port ${port}`.blue.bold)
})

// Create a redirect from HTTP to HTTPS if running HTTPS
if (runHttps) {
    const httpPort = 80
    http.createServer(function (req, res) {
        console.log(req.url)
        res.writeHead(301, {
            Location: "https://" + req.headers["host"] + req.url
        })
        res.end()
    }).listen(httpPort)
    console.log(`Server will redirect HTTP from port ${httpPort}`.blue.bold)
}

// Routing
app.use(express.static(path.join(__dirname, 'public')))

// Chatroom
let numUsers = 0

// Handle realtime comms
io.on('connection', (socket) => {
    let addedUser = false

    // when the client emits 'new message', this listens and executes
    socket.on('new message', (data) => {
        // we tell the client to execute 'new message'
        socket.broadcast.emit('new message', {
            username: socket.username,
            message: data,
        })
        
        console.log(`New message from ${socket.username}: ${data}`.green.bold)
    })

    // when the client emits 'add user', this listens and executes
    socket.on('add user', (username) => {
        if (addedUser) return

        // we store the username in the socket session for this client
        socket.username = username
        ++numUsers
        addedUser = true
        socket.emit('login', {
            numUsers: numUsers,
        })
        // echo globally (all clients) that a person has connected
        socket.broadcast.emit('user joined', {
            username: socket.username,
            numUsers: numUsers,
        })

        console.log(`Added user: ${socket.username}`.magenta.bold)
    })

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', () => {
        socket.broadcast.emit('typing', {
            username: socket.username,
        })

        console.log(`${socket.username} is typing.`.yellow)
    })

    // when the client emits 'stop typing', we broadcast it to others
    socket.on('stop typing', () => {
        socket.broadcast.emit('stop typing', {
            username: socket.username
        })
        console.log(`${socket.username} stopped typing.`.yellow)
    })

    // when the user disconnects.. perform this
    socket.on('disconnect', () => {
        if (addedUser) {
            --numUsers

            // echo globally that this client has left
            socket.broadcast.emit('user left', {
                username: socket.username,
                numUsers: numUsers,
            })

            console.log(`${socket.username} left.`.red.bold)
        }
    })
})