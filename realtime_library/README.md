# Realtime Libraries

If we use a library for realtime connections, we can abstract away our dependence upon the underlying transport technology. Why is this important? Earlier we would have used long-polling for realtime communications, then came websockets, HTTP/2 offers SSE, and HTTP/3 provides WebTransport. A library can abstract over all these transports and transparently utilize the best one available on the client's browser without a need for the programmer to create specific code browser- and server-side. It also provides a possible future-proofing of your code.

We will use the ever popular [socket.io](https://socket.io).

## Running the Demo

Just type `npm install` then run `npm start` and the program should be up and running.
